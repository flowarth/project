@extends('../template')
@section('title', 'Tambah Slider')

@section('content')

<div class="card">
  <div class="card-header">
    <h3 class="card-title">FORM SLIDER</h3>
  </div>
  <div class="card-body">
    <div class="container">

      <div class="col-lg-6">
        <form method="POST" action="{{url('slider/update', $slider->id)}}" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label >No Urut</label>
            <input type="number" class="form-control" name="no_urut" id="no_urut" placeholder="Masukan No Urut" value="{{ $slider->no_urut }}">
            <div class="text-danger">@error('no_urut'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label>Foto Lama</label>
            <img src="{{ asset('uploads/slider/'.$slider->foto) }}" class="img-thumbnail">
          </div>
          <div class="form-group">
            <label>Foto</label>
            <input type="file" class="form-control" name="foto" id="foto" placeholder="Masukan Alamat" >
            <div class="text-danger">@error('foto'){{ $message }}@enderror</div>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Submit</button>
          <a href="{{ url('slider') }}" class="btn btn-default btn-sm">Kembali</a>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection