@extends('../template')

@section('content')

@if(session('pesan'))
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('pesan') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    </div>
  </div>
</div>
@endif

<div class="card">
  <div class="card-header">
    <h3 class="card-title">LIST SLIDER</h3>
    <div class="float-right">
      <a href="{{ url('slider/tambah') }}" class="btn btn-primary btn-sm">Tambah</a>
    </div>
  </div>
  <div class="card-body">
    <div class="container">
      <table class="table table-bordered table-stripped">
        <thead>
          <tr>
            <th>No</th>
            <th>No_urut</th>
            <th>Foto</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($slider as $kat)
          <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $kat->no_urut }}</td>
            <td><img src="{{ asset('uploads/slider/'.$kat->foto) }}" class="img-thumbnail" width="200" height="200"></td>
            <td>
              <a href="{{ url('slider/edit',$kat->id) }}" class="btn btn-info btn-sm">Edit</a>
              <a href="{{ url('slider/delete', $kat->id) }}" class="btn btn-danger btn-sm">Delete</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection