@extends('../template')
@section('title', 'List Biodata')

@section('content')

@if(session('pesan'))
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('pesan') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
    </div>
  </div>
</div>
@endif

<div class="card">
  <div class="card-header">
    <h3 class="card-title">LIST BIODATA</h3>
    <div class="float-right">
      <a href="{{ url('biodata/tambah') }}" class="btn btn-primary btn-sm">Tambah</a>
    </div>
  </div>
  <div class="card-body">
    <div class="container">
      <table class="table table-bordered table-stripped">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Alamat</th>
            <th scope="col">Jurusan</th>
            <th scope="col">Email</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($biodata as $bio)
          <tr>
            <th scope="row">{{ $loop->iteration }}</th>
            <th scope="row">{{ $bio->nama }}</th>
            <th scope="row">{{ $bio->alamat }}</th>
            <th scope="row">{{ $bio->jurusan }}</th>
            <th scope="row">{{ $bio->email }}</th>
            <th scope="row">
              <a href="{{ url('biodata/edit',$bio->id) }}" class="btn btn-info btn-sm">Edit</a>
              <a href="{{ url('biodata/delete', $bio->id) }}" class="btn btn-danger btn-sm">Delete</a>
            </th>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection