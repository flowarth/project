@extends('../template')
@section('title', 'Tambah Biodata')

@section('content')

<div class="card">
  <div class="card-header">
    <h3 class="card-title">FORM BIODATA</h3>
  </div>
  <div class="card-body">
    <div class="container">

      <div class="col-lg-6">
        <form method="POST" action="{{url('biodata/form')}}">
          @csrf
          <div class="form-group">
            <label >Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama">
            <div class="text-danger">@error('nama'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukan Alamat" >
            <div class="text-danger">@error('alamat'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label>Jurusan</label>
            <input type="text" class="form-control" name="jurusan" id="jurusan" placeholder="Masukan Jurusan">
            <div class="text-danger">@error('jurusan'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" id="email" placeholder="Masukan Email">
            <div class="text-danger">@error('email'){{ $message }}@enderror</div>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Submit</button>
          <a href="{{ url('biodata') }}" class="btn btn-default btn-sm">Kembali</a>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection