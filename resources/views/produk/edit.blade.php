@extends('../template')
@section('title', 'Tambah Produk')

@section('content')

<div class="card">
  <div class="card-header">
    <h3 class="card-title">FORM PRODUK</h3>
  </div>
  <div class="card-body">
    <div class="container">

      <div class="col-lg-6">
        <form method="POST" action="{{url('produk/update', $produk->id)}}" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label >Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama" value="{{ $produk->nama }}">
            <div class="text-danger">@error('nama'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label >Harga</label>
            <input type="number" class="form-control" name="harga" id="harga" placeholder="Masukan Harga" value="{{ $produk->harga }}">
            <div class="text-danger">@error('harga'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label >Jumlah</label>
            <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Masukan Jumlah" value="{{ $produk->jumlah }}">
            <div class="text-danger">@error('jumlah'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label>Foto Lama</label>
            <img src="{{ asset('uploads/produk/'.$produk->foto) }}" class="img-thumbnail">
          </div>
          <div class="form-group">
            <label>Foto</label>
            <input type="file" class="form-control" name="foto" id="foto" placeholder="Masukan Alamat" >
            <div class="text-danger">@error('foto'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label >Kategori</label>
            <select class="form-control" name="kategori">
              <option value="">- Pilih -</option>
              @foreach($kategori as $kat)
              <option value="{{ $kat->id }}" {{ $produk->id_kategori == $kat->id ? 'selected' : '' }}>{{ $kat->nama }}</option>
              @endforeach
            </select>
            <div class="text-danger">@error('kategori'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label>Deskripsi</label>
            <textarea class="form-control" name="deskripsi" rows="3">{{ $produk->deskripsi }}</textarea>
            <div class="text-danger">@error('deskripsi'){{ $message }}@enderror</div>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Submit</button>
          <a href="{{ url('produk') }}" class="btn btn-default btn-sm">Kembali</a>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection