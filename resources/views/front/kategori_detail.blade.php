@extends('../template_front')

@section('content')

<div class="bg0 m-t-23 p-b-140 p-t-70">
	<div class="container">

		<div class="row isotope-grid">
			@foreach($produk as $p)
			<div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
				<!-- Block2 -->
				<div class="block2">
					<div class="block2-pic hov-img0">
						<img src="{{ asset('uploads/produk/'.$p->foto) }}" alt="IMG-PRODUCT">

						<a href="{{ url('product/'.$p->slug) }}" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04">
							Detail
						</a>
					</div>

					<div class="block2-txt flex-w flex-t p-t-14">
						<div class="block2-txt-child1 flex-col-l ">
							<a href="{{ url('product/'.$p->slug) }}" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
								{{ $p->nama }}
							</a>

							<span class="stext-105 cl3">
								{{ number_format($p->harga) }}
							</span>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>

	</div>
</div>

@endsection