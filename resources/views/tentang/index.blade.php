@extends('../template')

@section('content')

<div class="card">
	<div class="card-header">
		<h3 class="card-title">TENTANG</h3>
	</div>
	<div class="card-body">
		<div class="container">

			@if(session('pesan'))
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							{{ session('pesan') }}
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div>
					</div>
				</div>
			</div>
			@endif

			<form method="POST" action="{{url('tentang/edit')}}" enctype="multipart/form-data">
				@csrf

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label >Nama</label>
							<input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama" value="{{ $tentang->nama_aplikasi }}">
							<div class="text-danger">@error('nama'){{ $message }}@enderror</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label >Alamat</label>
							<input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukan Alamat" value="{{ $tentang->alamat }}">
							<div class="text-danger">@error('alamat'){{ $message }}@enderror</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label >Telp</label>
							<input type="text" class="form-control" name="telp" id="telp" placeholder="Masukan Telp" value="{{ $tentang->telp }}">
							<div class="text-danger">@error('telp'){{ $message }}@enderror</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label >Email</label>
							<input type="text" class="form-control" name="email" id="email" placeholder="Masukan Email" value="{{ $tentang->email }}">
							<div class="text-danger">@error('email'){{ $message }}@enderror</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label >Instagram</label>
							<input type="text" class="form-control" name="instagram" id="instagram" placeholder="Link Instagram" value="{{ $tentang->instagram }}">
							<div class="text-danger">@error('instagram'){{ $message }}@enderror</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label >Facebook</label>
							<input type="text" class="form-control" name="facebook" id="facebook" placeholder="Link Facebook" value="{{ $tentang->facebook }}">
							<div class="text-danger">@error('facebook'){{ $message }}@enderror</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label >Deskripsi</label>
							<textarea class="form-control" name="deskripsi" id="deskripsi" rows="15">{{ $tentang->deskripsi }}</textarea>
							<div class="text-danger">@error('deskripsi'){{ $message }}@enderror</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Logo</label>
							<input type="file" class="form-control" name="logo" id="logo" placeholder="Masukan Alamat" >
							<div class="text-danger">@error('logo'){{ $message }}@enderror</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Favicon</label>
							<input type="file" class="form-control" name="favicon" id="favicon" placeholder="Masukan Alamat" >
							<div class="text-danger">@error('favicon'){{ $message }}@enderror</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Logo Sebelumnya</label>
							<img src="{{ asset('uploads/tentang/'.$tentang->logo) }}" class="img-thumbnail">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Favicon Sebelumnya</label>
							<img src="{{ asset('uploads/tentang/'.$tentang->favicon) }}" class="img-thumbnail">
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-primary btn-sm">Submit</button>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		tinymce.init({
			selector: '#deskripsi'
		});
	})
</script>

@endsection