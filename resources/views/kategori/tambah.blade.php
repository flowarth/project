@extends('../template')
@section('title', 'Tambah Kategori')

@section('content')

<div class="card">
  <div class="card-header">
    <h3 class="card-title">FORM KATEGORI</h3>
  </div>
  <div class="card-body">
    <div class="container">

      <div class="col-lg-6">
        <form method="POST" action="{{url('kategori/form')}}" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label >Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama">
            <div class="text-danger">@error('nama'){{ $message }}@enderror</div>
          </div>
          <div class="form-group">
            <label>Foto</label>
            <input type="file" class="form-control" name="foto" id="foto" placeholder="Masukan Alamat" >
            <div class="text-danger">@error('foto'){{ $message }}@enderror</div>
          </div>
          <button type="submit" class="btn btn-primary btn-sm">Submit</button>
          <a href="{{ url('kategori') }}" class="btn btn-default btn-sm">Kembali</a>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection