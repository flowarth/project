<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
	protected $table = 'table_kategori';
    protected $fillable = ['nama', 'slug', 'foto'];

    public function produk(){
		return $this->hasOne('App\Models\Produk');
	}
}
