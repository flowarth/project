<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tentang extends Model
{
    Protected $table = 'table_tentang';
    Protected $fillable = ['nama_aplikasi', 'deskripsi', 'alamat', 'telp', 'email', 'logo', 'favicon', 'instagram', 'facebook'];
}
