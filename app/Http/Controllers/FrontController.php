<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Slider, Produk, Kategori, Tentang};

class FrontController extends Controller{
    public function index(){
    	$slider = Slider::all();
    	$kategori = Kategori::limit(3)->get();
    	$produk = Produk::limit(3)->get();
        $tentang = Tentang::where('id', 1)->first();

    	return view('template_front', compact('slider', 'kategori', 'produk', 'tentang'));
    }

    public function produk(){
        $produk = Produk::all();
        $tentang = Tentang::where('id', 1)->first();

        return view('front/produk', compact('produk', 'tentang'));
    }

    public function produk_detail($slug){
    	$produk = Produk::select('table_produk.*', 'table_kategori.nama as nama_kategori')->where('table_produk.slug', $slug)->join('table_kategori', 'table_kategori.id', '=', 'table_produk.id_kategori')->first();
        $tentang = Tentang::where('id', 1)->first();
        // $produk = Produk::where('slug', $slug)->first();

    	return view('front/produk_detail', compact('produk', 'tentang'));
    }

    public function kategori(){
        $kategori = Kategori::all();
        $tentang = Tentang::where('id', 1)->first();

        return view('front/kategori', compact('kategori', 'tentang'));
    }

    public function kategori_detail($slug){
        $id_kategori = Kategori::where('slug', $slug)->first();
        $produk = Produk::where('id_kategori', $id_kategori->id)->get();
        $tentang = Tentang::where('id', 1)->first();

        return view('front/kategori_detail', compact('produk', 'tentang'));
    }

    public function about(){
        $tentang = Tentang::where('id', 1)->first();

        return view('front/about', compact('tentang'));
    }
}
