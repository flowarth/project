<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\{Produk, Kategori};

class ProdukController extends Controller {
	public function index(){
		$produk = produk::all();
		return view('produk.list', compact('produk'));
	}

	public function tambah(){
		$kategori = Kategori::all();
		return view('produk.tambah', compact('kategori'));
	}

	public function form(Request $request){
		$this->validasi('create');

		$foto = $request->foto;
		$new_foto = Str::random(16).'.'.$foto->extension();
		$foto->move('uploads/produk/', $new_foto);

		Produk::create([
			'nama' => $request->nama,
			'slug'=> \Str::slug($request->nama),
			'harga' => $request->harga,
			'jumlah' => $request->jumlah,
			'foto'=> $new_foto,
			'id_kategori' => $request->kategori,
			'deskripsi' => $request->deskripsi,
		]);

		return redirect('produk')->with('pesan', 'Berhasil disimpan');
	}

	public function edit($id){
		$produk = produk::findOrFail($id);
		$kategori = Kategori::all();

		return view('produk.edit', compact('produk', 'kategori'));
	}

	public function update($id){
		$this->validasi('update');
		$produk = produk::findOrFail($id);

		if (Request()->foto != '') {
			$foto = Request()->foto;
			$new_foto = Str::random(16).'.'.$foto->extension();
			$foto->move('uploads/produk/', $new_foto);
			unlink('uploads/produk/'.$produk->foto);
		}

		$produk->nama = Request()->nama;
		$produk->harga = Request()->harga;
		$produk->jumlah = Request()->jumlah;
		$produk->foto = Request()->foto != '' ? $new_foto : $produk->foto;
		$produk->id_kategori = Request()->kategori;
		$produk->deskripsi = Request()->deskripsi;
		$produk->save();

		return redirect('produk')->with('pesan', 'Berhasil disimpan');
	}

	public function delete($id){
		$produk = produk::findOrFail($id);
		unlink('uploads/produk/'.$produk->foto);
		$produk->delete();

		return redirect('produk')->with('pesan', 'Berhasil dihapus');
	}

	public function validasi($type){
		if ($type == 'create') {
			Request()->validate([
				'nama' => 'required',
				'harga' => 'required',
				'jumlah' => 'required',
				'foto' => 'required',
				'kategori' => 'required',
				'deskripsi' => 'required',
			],
			[
				'nama.required' => 'Nama wajib isi',
				'foto.required' => 'Foto wajib isi',
			]);
		} else {
			Request()->validate([
				'nama' => 'required',
				'harga' => 'required',
				'jumlah' => 'required',
				'kategori' => 'required',
				'deskripsi' => 'required',
			],
			[
				'nama.required' => 'Nama wajib isi'
			]);
		}
	}
}
