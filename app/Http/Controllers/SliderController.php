<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Slider;

class SliderController extends Controller {
	public function index(){
		$slider = slider::all();

		return view('slider.list', compact('slider'));
	}

	public function tambah(){
		return view('slider.tambah');
	}

	public function form(Request $request){
		$this->validasi('create');

		$foto = $request->foto;
		$new_foto = Str::random(16).'.'.$foto->extension();
		$foto->move('uploads/slider/', $new_foto);

		slider::create([
			'no_urut' => $request->no_urut,
			'foto'=> $new_foto,
		]);

		return redirect('slider')->with('pesan', 'Berhasil disimpan');
	}

	public function edit($id){
		$slider = slider::findOrFail($id);

		return view('slider.edit', compact('slider'));
	}

	public function update($id){
		$this->validasi('update');
		$slider = slider::findOrFail($id);

		if (Request()->foto != '') {
			$foto = Request()->foto;
			$new_foto = Str::random(16).'.'.$foto->extension();
			$foto->move('uploads/slider/', $new_foto);
			unlink('uploads/slider/'.$slider->foto);
		}

		$slider->no_urut = Request()->no_urut;
		$slider->foto = Request()->foto != '' ? $new_foto : $slider->foto;
		$slider->save();

		return redirect('slider')->with('pesan', 'Berhasil disimpan');
	}

	public function delete($id){
		$slider = slider::findOrFail($id);
		unlink('uploads/slider/'.$slider->foto);
		$slider->delete();

		return redirect('slider')->with('pesan', 'Berhasil dihapus');
	}

	public function validasi($type){
		if ($type == 'create') {
			Request()->validate([
				'no_urut' => 'required',
				'foto' => 'required',
			]);
		} else {
			Request()->validate([
				'no_urut' => 'required'
			]);
		}
	}
}
