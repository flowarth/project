<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Biodata;
use Illuminate\Support\Facades\DB;

class BiodataController extends Controller{
    public function index(){
        // $biodata = DB::table('biodata')->get();
        $biodata = Biodata::all();

        return view('biodata/list', compact('biodata'));
    }

    public function tambah(){
        return view('biodata/tambah');
    }

    public function form(Request $request){
    // $biodata = $request->all();
        $this->validasi();

        Biodata::create([
            'nama' => $request->nama,
            'alamat'=> $request->alamat,
            'jurusan'=> $request->jurusan,
            'email'=> $request->email
        ]);

    // Biodata::create($biodata);

        return redirect('/')->with('pesan', 'Berhasil disimpan');
    }

    public function edit($id){
        $biodata = Biodata::findOrFail($id);
        // $biodata = Biodata::where('id', $id)->first();
        // $biodata = Biodata::find($id);

        return view('biodata/edit', compact('biodata'));

    }

    public function update($id){
        $this->validasi();

        $biodata = Biodata::findOrFail($id);

        $biodata->nama = Request()->nama;
        $biodata->alamat = Request()->alamat;
        $biodata->jurusan = Request()->jurusan;
        $biodata->email = Request()->email;
        $biodata->save();

        return redirect('/')->with('pesan', 'Berhasil diubah');
    }

    public function delete($id){
        $biodata = Biodata::findOrFail($id);
        $biodata->delete();

        return redirect('/')->with('pesan', 'Berhasil dihapus');
    }

    public function validasi(){
        Request()->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'jurusan' => 'required',
            'email' => 'required',
        ],
        [
            'nama.required' => 'Nama wajib isi',
            'alamat.required' => 'Alamat wajib isi',
            'jurusan.required' => 'Jurusan wajib isi',
            'email.required' => 'Email wajib isi',
        ]);
    }
}
