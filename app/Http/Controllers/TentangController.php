<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Tentang;

class TentangController extends Controller
{
    public function index(){
    	$tentang = Tentang::where('id', 1)->first();

    	return view('tentang/index', compact('tentang'));
    }

    public function edit(){
    	$tentang = Tentang::where('id', 1)->first();

    	if (Request()->logo != '') {
    		$logo = Request()->logo;
			$new_logo = Str::random(16).'.'.$logo->extension();
			$logo->move('uploads/tentang/', $new_logo);
			
			if ($tentang->logo != '') {
				unlink('uploads/tentang/'.$tentang->logo);
			}
    	}

    	if (Request()->favicon != '') {
    		$favicon = Request()->favicon;
			$new_favicon = Str::random(16).'.'.$favicon->extension();
			$favicon->move('uploads/tentang/', $new_favicon);
			
			if ($tentang->favicon != '') {
				unlink('uploads/tentang/'.$tentang->favicon);
			}
    	}

    	$tentang->nama_aplikasi = Request()->nama;
    	$tentang->alamat = Request()->alamat;
    	$tentang->deskripsi = Request()->deskripsi;
    	$tentang->telp = Request()->telp;
    	$tentang->email = Request()->email;
    	$tentang->instagram = Request()->instagram;
    	$tentang->facebook = Request()->facebook;
    	$tentang->logo = Request()->logo != '' ? $new_logo : $tentang->logo;
    	$tentang->favicon = Request()->favicon != '' ? $new_favicon : $tentang->favicon;
    	$tentang->save();

    	return redirect('tentang')->with('pesan', 'Berhasil disimpan');
    }
}
