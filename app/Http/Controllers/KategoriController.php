<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Kategori;

class KategoriController extends Controller {
	public function index(){
		$kategori = Kategori::all();

		return view('kategori.list', compact('kategori'));
	}

	public function tambah(){
		return view('kategori.tambah');
	}

	public function form(Request $request){
		$this->validasi('create');

		$foto = $request->foto;
		$new_foto = Str::random(16).'.'.$foto->extension();
		$foto->move('uploads/kategori/', $new_foto);

		Kategori::create([
			'nama' => $request->nama,
			'slug'=> \Str::slug($request->nama),
			'foto'=> $new_foto,
		]);

		return redirect('kategori')->with('pesan', 'Berhasil disimpan');
	}

	public function edit($id){
		$kategori = Kategori::findOrFail($id);

		return view('kategori.edit', compact('kategori'));
	}

	public function update($id){
		$this->validasi('update');
		$kategori = Kategori::findOrFail($id);

		if (Request()->foto != '') {
			$foto = Request()->foto;
			$new_foto = Str::random(16).'.'.$foto->extension();
			$foto->move('uploads/kategori/', $new_foto);
			unlink('uploads/kategori/'.$kategori->foto);
		}

		$kategori->nama = Request()->nama;
		$kategori->foto = Request()->foto != '' ? $new_foto : $kategori->foto;
		$kategori->save();

		return redirect('kategori')->with('pesan', 'Berhasil disimpan');
	}

	public function delete($id){
		$kategori = Kategori::findOrFail($id);
		unlink('uploads/kategori/'.$kategori->foto);
		$kategori->delete();

		return redirect('kategori')->with('pesan', 'Berhasil dihapus');
	}

	public function validasi($type){
		if ($type == 'create') {
			Request()->validate([
				'nama' => 'required',
				'foto' => 'required',
			],
			[
				'nama.required' => 'Nama wajib isi',
				'foto.required' => 'Foto wajib isi',
			]);
		} else {
			Request()->validate([
				'nama' => 'required'
			],
			[
				'nama.required' => 'Nama wajib isi'
			]);
		}
	}
}
