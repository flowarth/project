<?php

use Illuminate\Support\Facades\Route;

Auth::routes();
Route::get('/', 'FrontController@index');
Route::get('product', 'FrontController@produk');
Route::get('product/{slug}', 'FrontController@produk_detail');
Route::get('categori/{slug}', 'FrontController@kategori_detail');
Route::get('categori', 'FrontController@kategori');
Route::get('about', 'FrontController@about');

Route::group(['middleware' => 'auth'], function () {

	Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

	Route::get('biodata','BiodataController@index');
	Route::get('biodata/tambah','BiodataController@tambah');
	Route::post('biodata/form','BiodataController@form');
	Route::get('biodata/edit/{id}','BiodataController@edit');
	Route::post('biodata/update/{id}','BiodataController@update');
	Route::get('biodata/delete/{id}','BiodataController@delete');

	Route::get('kategori', 'KategoriController@index');
	Route::get('kategori/tambah','KategoriController@tambah');
	Route::post('kategori/form','KategoriController@form');
	Route::get('kategori/edit/{id}','KategoriController@edit');
	Route::post('kategori/update/{id}','KategoriController@update');
	Route::get('kategori/delete/{id}','KategoriController@delete');

	Route::get('produk', 'ProdukController@index');
	Route::get('produk/tambah','ProdukController@tambah');
	Route::post('produk/form','ProdukController@form');
	Route::get('produk/edit/{id}','ProdukController@edit');
	Route::post('produk/update/{id}','ProdukController@update');
	Route::get('produk/delete/{id}','ProdukController@delete');

	Route::get('slider', 'SliderController@index');
	Route::get('slider/tambah','SliderController@tambah');
	Route::post('slider/form','SliderController@form');
	Route::get('slider/edit/{id}','SliderController@edit');
	Route::post('slider/update/{id}','SliderController@update');
	Route::get('slider/delete/{id}','SliderController@delete');

	Route::get('tentang', 'TentangController@index');
	Route::post('tentang/edit', 'TentangController@edit');
});