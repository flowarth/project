<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTentang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_tentang', function (Blueprint $table) {
            $table->id();
            $table->string('nama_aplikasi');
            $table->text('deskripsi');
            $table->text('alamat');
            $table->string('telp');
            $table->string('email');
            $table->string('logo');
            $table->string('favicon');
            $table->string('instagram');
            $table->string('facebook');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_tentang');
    }
}
